#!/bin/bash
echo -e "section_start:`date +%s`:my_first_section[collapsed=true]\r\e[0KHeader of the 1st collapsible section"
echo 'this line should be hidden when collapsed'
echo -e "section_end:`date +%s`:my_first_section\r\e[0K"

echo Going to second section

echo -e "section_start:`date +%s`:second_section[collapsed=true]\rHeader of the 2nd collapsible section"
echo 'this line should be hidden when collapsed'
echo -e "section_end:`date +%s`:second_section\r"
