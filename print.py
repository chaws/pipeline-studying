import time
from colorama import Fore, Back, Style

now = lambda: int(time.time())

print(f"section_start:{now()}:first_section[collapsed=true]\r")
print(Fore.RED + 'some red text')
print(Back.GREEN + 'and with a green background')
print(Style.DIM + 'and in dim text')
print(Style.RESET_ALL)
print('back to normal now')
print(f"section_end:{now()}:first_section\r")
